# Deployment of fox Services

Follwoing figure discussed the architecture of the fox platfrom.

![Alt text](fox-architecture.png?raw=true "fox platfrom architecture")

## Services

There are three main servies;

```
1. elassandra
2. aplos
3. gateway
```

## Configuration

Change `host.docker.local` field in `.env` file to local machines ip. also its possible to add a host entry to `/etc/hosts` file by overriding `host.docker.local` with local machines ip. following is an example of `/etc/hosts` file.

```
10.4.1.104    host.docker.local
```

Give write permission to `/private/var/services/connect/elassandra` directory in the server. following is the way to give the permission,

```
sudo mkdir /private/var/services/connect/elassandra
sudo chmod -R 777 /private
```

## Deploy services

Start services in following order;

```
docker-compose up -d elassandra
docker-compose up -d aplos
docker-compose up -d gateway
docker-compose up -d web
```

\*\* After hosting website, it can be reached on `<Ip address of the Vm>:4300`.

## Connect apis

gateway service will start a REST api on `7654` port. For an example if your machines ip is `10.4.1.104` the apis can be access via `10.4.1.104:7654/api/<apiname>`.Following are the available rest api end points and their specifications,

\*\* Open `7654` and `4300` port on VM for public

#### 1. User details

```
# request
curl -XPOST "http://localhost:7654/api/user" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "111110",
  "execer": "admin:admin",
  "messageType": "addUser",
  "userMobile": "0708878997",
  "userName": "Sahansa",
  "gender": "male"
}
'

# reply
{"code":2021,"msg":"user added", "userMobile": "0708878997","userName": "Sahansa","gender": "male"}
```

#### 2. get User Details

```
# request
curl -XPOST "http://localhost:7654/api/user" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "111110",
  "execer": "admin:admin",
  "messageType": "getUserDetails",
  "userMobile": "0708878997",
  "userName": "Sahansa",
  "gender": "male"
}
'

# reply
{"id": "111110","execer": "admin:admin","messageType": "getUserDetails","userMobile": "0708878997","userName": "Sahansa","gender": "male","timestamp":"2021-07-12 20:55:21.308"}
```
